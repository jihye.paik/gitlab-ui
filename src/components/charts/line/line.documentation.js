import description from './line.md';
import examples from './examples';

export default {
  followsDesignSystem: true,
  description,
  examples,
};
