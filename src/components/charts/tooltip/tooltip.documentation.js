import description from './tooltip.md';
import examples from './examples';

export default {
  followsDesignSystem: true,
  description,
  examples,
  bootstrapComponent: 'b-popover',
};
