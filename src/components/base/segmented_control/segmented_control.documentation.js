import description from './segmented_control.md';
import examples from './examples';

export default {
  followsDesignSystem: true,
  description,
  examples,
  bootstrapComponent: 'b-form-radio-group',
};
