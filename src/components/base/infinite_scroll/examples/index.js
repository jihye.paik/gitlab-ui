import InfiniteScrollBasicExample from './infinite_scroll.basic.example.vue';

export default [
  {
    name: 'Basic',
    items: [
      {
        id: 'infinite-scroll-basic',
        name: 'Basic',
        description: 'Basic Infinite Scroll',
        component: InfiniteScrollBasicExample,
      },
    ],
  },
];
