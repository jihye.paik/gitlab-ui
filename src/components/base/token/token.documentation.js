import examples from './examples';

export default {
  followsDesignSystem: true,
  examples,
  events: [
    {
      event: 'close',
      description: 'Emitted when x is clicked',
    },
  ],
};
