import description from './drawer.md';
import examples from './examples';

export default {
  followsDesignSystem: true,
  description,
  examples,
};
