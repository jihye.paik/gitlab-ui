# [8.8.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v8.7.0...v8.8.0) (2019-12-17)


### Features

* Add xported utils ([1d35af2](https://gitlab.com/gitlab-org/gitlab-ui/commit/1d35af230a741503162e1f295e9737fcac597d4c))

# [8.7.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v8.6.3...v8.7.0) (2019-12-17)


### Features

* Init card component ([783121d](https://gitlab.com/gitlab-org/gitlab-ui/commit/783121dcb68a7d6d6b28b09ba29869de6610df46))

## [8.6.3](https://gitlab.com/gitlab-org/gitlab-ui/compare/v8.6.2...v8.6.3) (2019-12-16)


### Bug Fixes

* **search:** fix search components with gitlab CSS ([4c65fdd](https://gitlab.com/gitlab-org/gitlab-ui/commit/4c65fdd1485a0eb7f18eca09903309e8f34e60f0))

## [8.6.2](https://gitlab.com/gitlab-org/gitlab-ui/compare/v8.6.1...v8.6.2) (2019-12-13)


### Reverts

* feat: add and style badges ([b78b497](https://gitlab.com/gitlab-org/gitlab-ui/commit/b78b4976d2e47e4d7c37393d460fc134c07edbf6))
